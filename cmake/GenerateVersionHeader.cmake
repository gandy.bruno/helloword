if(GIT_EXECUTABLE)
  #get_filename_component( "${PROJECT_SOURCE_DIR}/include/version.h.in" DIRECTORY)
  # Generate a git-describe version string from Git repository tags
  execute_process(
    COMMAND ${GIT_EXECUTABLE} describe --tags --match "V-*"
    WORKING_DIRECTORY ${PROJECT_SOURCE_DIR}
    OUTPUT_VARIABLE MY_PROGRAM_VERSION
    RESULT_VARIABLE GIT_DESCRIBE_ERROR_CODE
    OUTPUT_STRIP_TRAILING_WHITESPACE
    )
  if(NOT GIT_DESCRIBE_ERROR_CODE)
    string(REPLACE "V-" "" MY_PROGRAM_VERSION ${MY_PROGRAM_VERSION})
  else()
# Final fallback: Just use a bogus version string that is semantically older
# than anything else and spit out a warning to the developer.
  set(MY_PROGRAM_VERSION 0.0.0)
  message(WARNING "Failed to determine the version from Git tags. Using default version \"${FOOBAR_VERSION}\".")
  endif()
   
  set(CMAKE_PROJECT_VERSION ${MY_PROGRAM_VERSION})
  string(REPLACE "." ";" VERSION_LIST ${MY_PROGRAM_VERSION})  
  list(GET VERSION_LIST 0 CMAKE_PROJECT_VERSION_MAJOR)
  list(GET VERSION_LIST 1 CMAKE_PROJECT_VERSION_MINOR)
  list(GET VERSION_LIST 2 CMAKE_PROJECT_VERSION_PATCH)

endif()

configure_file(${CMAKE_CURRENT_SOURCE_DIR}/cmake/version.h.in  ${CMAKE_BINARY_DIR}/Version.h @ONLY)